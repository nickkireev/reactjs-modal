var TemplateWindow = React.createClass({
  reRender: function() {
    this.setState(this.getInitialState());
  },
  hideModal: function() {
    this.setState({showModal: false});
  },
  getInitialState: function() {
    return {
      data: [],
      showModal: true,
      firstTemplate: false,
      secondTemplate: false,
      selectedItem1: 0,
      selectedItem2: 0
    };
  },
  setFirstTemplate: function(isSel,num) {
      this.setState({
        firstTemplate:  isSel,
        selectedItem1: num
      });
  },
  setSecondTemplate: function(isSel,num) {
      this.setState({
        secondTemplate:  isSel,
        selectedItem2: num
      });
  },
  render: function() {
    var checkedItem1 = this.state.firstTemplate && !this.state.secondTemplate;
    var checkedItem2 = !this.state.firstTemplate && this.state.secondTemplate;
    var checkedItem3 = this.state.firstTemplate && this.state.secondTemplate;
    console.log('TemplateWindow', checkedItem1,checkedItem2,checkedItem3);

      var modal = <div className="modal-content">
        <div className="modal-header">
          <h4 className="modal-title">Assign template</h4>
          <button type="button" className="close-btn" onClick={this.hideModal}>×</button>
        </div>
        <div className="modal-body">
          <form action="#" className="form">
            <div className="fieldset">
              <div className="legend">Select template</div>
              <SelectTemplate  url="/api/data1" selectId={1} isSelected={this.setFirstTemplate} selectedItem={this.state.selectedItem1}/>
              <SelectTemplate  url="/api/data2" selectId={2} isSelected={this.setSecondTemplate} selectedItem={this.state.selectedItem2}/>
            </div>
            <div className="fieldset">
              <div className="legend">Assign template type</div>
              <div className="form-group flex-row">
                <AssignTemplate url="/api/radio1" checkedItem={checkedItem1}/>
                <AssignTemplate url="/api/radio2" checkedItem={checkedItem2}/>
                <AssignTemplate url="/api/radio3" checkedItem={checkedItem3}/>
              </div>
            </div>
            <FooterTemplate accept={this.reRender} hide={this.hideModal}/>
          </form>
        </div>
      </div>
    return (
        <div className="flex-container">
          <div id="modalWrapper" className={this.state.showModal ? 'modal' : 'modal hidden'}>
            <div className="modal-dialog">
              {modal}
            </div>
          </div>
        </div>
    );
  }
});
var SelectTemplate = React.createClass({
  loadDataFromServer: function () {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function (data) {
        this.setState({data: data[0]});
      }.bind(this),
      error: function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function () {
    return {
      data: [],
      val: 0,
      timestamp: Date.now(),
      selectKey: 'sel'
    };
  },
  shouldComponentUpdate: function(nextProps, nextState) {
    return true;
  },
  componentDidMount: function () {
    this.loadDataFromServer();
  },
  handleChange: function (event) {
    this.props.isSelected( event.target.value != 0, event.target.value);
  },
  render: function () {
    var options = [<option key={this.state.timestamp - 1} value={0}> -- select an option -- </option>];
    for (var i in this.state.data.options) {
      options.push(
          <option key={this.state.timestamp + i} value={i}>{this.state.data.options[i]}</option>
      );
    }
    return (
        <div className="form-group flex-row">
          <label for={this.state.data.id}>{this.state.data.name}</label>
          <select className="select-form" key={this.state.selectKey} onChange={this.handleChange} value={this.props.selectedItem} id={this.state.data.id}>
            {options}
          </select>
        </div>
    );
  }
});

var AssignTemplate = React.createClass({
  loadDataFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({radio: data[0]});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {
      radio: []
    };
  },
  componentDidMount: function() {
    this.loadDataFromServer();
    //setInterval(this.loadDataFromServer, this.props.pollInterval);
  },
  render: function() {

    var checked = this.props.checkedItem;
      return (
        <div className="radio">
          <label>
            <input className="radio"
                   type="radio"
                   name={this.state.radio.name}
                   id={this.state.radio.id}
                   checked={checked}
                />
            {this.state.radio.text}
          </label>
        </div>
      );
  }
});
var FooterTemplate = React.createClass({
  resetAll: function() {
    this.props.accept();
  },
  hideAll: function() {
    this.props.hide();
  },
  render: function() {
    return (
        <div className="button-container">
          <button type="submit" className="button btn-info" onClick={this.resetAll}>Assign template</button>
          <button type="button" className="button btn-info modal-close" onClick={this.hideAll}>Cancel</button>
        </div>
    );
  }
});
ReactDOM.render(
  <TemplateWindow url="/api/data" pollInterval={2000} />,
  document.getElementById('content')
);
