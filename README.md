## ReactJS, FlexBox

tecomgroup.ru

### Tестовое задание

**Требуется разработать:**

1. Модальное окно с "резиновой" сеткой (см. прикрепленный файл [изображение]). Т.е. при изменении ширины окна (изменением аттрибута width класса родителя) текстовые элементы могут сжиматься до нескольких букв. Первые буквы текста должны отображаться как многоточие "...".

2. Верстка окна должна производится с использованием flex + css3.

3. Важно сверстать сетку в соотв. с макетом (интересует только модальное окно «Assign template»). Важны внешние и внутренние отступы, масштабируемость окна.

4. Шрифты, цвета не принципиальны.

Будет плюсом:

* Создать React компоненты (template window, footer, select_template, assign_template) на основе статической верстки со следующим поведением:

    * Выбор из выпадающего списка «Conditions»  проставляет radio «Assign Conditions»

    * Выбор из выпадающего списка «Notifications»  проставляет radio «Assign Notifications»

    * Если выбраны оба пункта, сделать активным radio «Assign both»

    * Нажатие на «Assign template» сбрасывает на дефолтные, «Cancel» закрывает окно

### Запуск

```sh
npm install
node server.js
```

### Developer
[@nickkireev]

[изображение]: <http://i.imgur.com/hkTWaSM.png>
[@nickkireev]: <https://bitbucket.org/nickkireev>
